﻿using NKSLK.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using NKSLK.EF;
using System.Globalization;
using System.Data.SqlClient;
using System.Data;

namespace NKSLK.DAO
{
    public class StatisticsDAO
    {
        NKSLKDbContext db = null;
        private static string connectionString = "data source=PC-98\\MSQSQLSERVER039;initial catalog=NKSLK;integrated security=True;" +
                                                 "MultipleActiveResultSets=True;App=EntityFramework";

        public StatisticsDAO()
        {
            db = new NKSLKDbContext();
        }

        /* Lay ra danh sach ID va HoTen cong nhan*/
        public List<LabourModel> GetLabourList()
        {
            List<LabourModel> list = new List<LabourModel>() { new LabourModel() { ID_CONGNHAN = 0, HoTen = "Tất cả" } };
            list.AddRange((from c in db.CONGNHANs
                           select new LabourModel
                           {
                               ID_CONGNHAN = c.ID_CONGNHAN,
                               HoTen = "NV" + (c.ID_CONGNHAN < 10 ? "00" + c.ID_CONGNHAN.ToString() :
                                               c.ID_CONGNHAN < 100 ? "0" + c.ID_CONGNHAN.ToString() :
                                               c.ID_CONGNHAN.ToString()) + " - " + c.HoTen
                           }).ToList());
            return list;
        }

        /* Thong ke cong viec co nhieu NKSLK nhat */
        public IEnumerable<MostPrevalentWorkModel> GetMostPrevalentWorks(string criterion, string selectedDate, string order)
        {
            List<MostPrevalentWorkModel> list = new List<MostPrevalentWorkModel>();

            // Tao ket noi toi csdl
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;

            // Loc du lieu theo tuan hoac thang
            string subquery = "";
            if (criterion != "All")
            {
                subquery = "and n.NgayThucHienKhoan between dbo.getFirstDayOf" + criterion +
                         "(@date) and dbo.getLastDayOf" + criterion + "(@date) ";
                command.Parameters.Add("@date", SqlDbType.DateTime);
                if (criterion == "Week")
                {
                    command.Parameters["@date"].Value = DateTime.ParseExact(selectedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    command.Parameters["@date"].Value = DateTime.ParseExact(selectedDate, "MM/yyyy", CultureInfo.InvariantCulture);
                }
            }

            // Tao cau lenh truy van lay du lieu tu nhung bang lien quan
            string query = "select c.ID_CONGVIEC, c.TenCongViec, count(ctcv.ID_CONGVIEC) as 'SoLuong' " +
                           "from CONGVIEC c, CHITIETCONGVIEC ctcv, NKSLK n " +
                           "where c.ID_CONGVIEC = ctcv.ID_CONGVIEC and ctcv.ID_NKSLK = n.ID_NKSLK " + subquery +
                           "group by c.ID_CONGVIEC, c.TenCongViec ";
         
            // Sap xep du lieu
            switch (order)
            {
                case "ID_asc": query += "order by c.ID_CONGVIEC "; break;
                case "ID_desc": query += "order by c.ID_CONGVIEC desc "; break;
                case "Name_asc": query += "order by c.TenCongViec "; break;
                case "Name_desc": query += "order by c.TenCongViec desc "; break;
                case "Quantity_asc": query += "order by SoLuong "; break;
                default: query += "order by SoLuong desc "; break;
            }

            // Tien hanh truy van
            command.CommandText = query;
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new MostPrevalentWorkModel()
                    {
                        ID_CONGVIEC = "NV" + (reader.GetInt32(0) < 10 ? "00" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0) < 100 ? "0" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0).ToString()),
                        TenCongViec = reader.GetString(1),
                        SoLuong = reader.GetInt32(2)
                    });
                }
            }
            connection.Close();

            // Chuyen sang dang IEnumerable
            IEnumerable<MostPrevalentWorkModel> model = list;
            return model;
        }

        /* Thong ke cong nhan lam viec ca 3 */
        public IEnumerable<ThirdShiftModel> GetThirdShifts(string criterion, string selectedDate, string order)
        {
            List<ThirdShiftModel> list = new List<ThirdShiftModel>();

            // Tao ket noi toi csdl
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
          
            // Tao cau lenh truy van lay du lieu tu nhung bang lien quan
            string query = "select distinct c.ID_CONGNHAN, c.HoTen " +
                           "from CONGNHAN c, CHITIETCONGNHAN ctcn, CHITIETCONGVIEC ctcv, NKSLK n " +
                           "where c.ID_CONGNHAN = ctcn.ID_CONGNHAN and ctcn.ID_CTCONGVIEC = ctcv.ID_CTCONGVIEC " +
                           "and ctcv.ID_NKSLK = n.ID_NKSLK and ctcn.GioTanLam <= '06:00:00' ";

            // Loc du lieu theo tuan hoac thang
            if (criterion != "All")
            {
                query += "and n.NgayThucHienKhoan between dbo.getFirstDayOf" + criterion +
                         "(@date) and dbo.getLastDayOf" + criterion + "(@date) ";
                command.Parameters.Add("@date", SqlDbType.DateTime);
                if (criterion == "Week")
                {
                    command.Parameters["@date"].Value = DateTime.ParseExact(selectedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    command.Parameters["@date"].Value = DateTime.ParseExact(selectedDate, "MM/yyyy", CultureInfo.InvariantCulture);
                }
            }

            // Sap xep du lieu
            switch (order)
            {
                case "ID_desc": query += "order by ID_CONGNHAN desc "; break;
                case "Name_asc": query += "order by c.HoTen "; break;
                case "Name_desc": query += "order by c.HoTen desc "; break;
                default: query += "order by ID_CONGNHAN "; break;
            }

            // Tien hanh truy van
            command.CommandText = query;
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new ThirdShiftModel()
                    {
                        ID_CONGNHAN = "NV" + (reader.GetInt32(0) < 10 ? "00" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0) < 100 ? "0" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0).ToString()),
                        HoTen = reader.GetString(1)
                    });
                }
            }
            connection.Close();

            // Chuyen sang dang IEnumerable
            IEnumerable<ThirdShiftModel> model = list;
            return model;
        }

        /* Thong ke NKSLK cua cong nhan theo tuan, thang */
        public IEnumerable<NKSLKModel> GetNKSLKs(string criterion, string selectedDate, string search, int idLabour, string order)
        {
            List<NKSLKModel> list = new List<NKSLKModel>();

            // Tao ket noi toi csdl
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;

            // Tao cau lenh truy van lay du lieu tu nhung bang lien quan
            string query = "select c.ID_CONGNHAN, c.HoTen, n.NgayThucHienKhoan, ctcn.GioVaoLam, ctcn.GioTanLam, " +
                           "cv.TenCongViec, s.TenSanPham, ctcv.SanLuong, ctcv.SoLoSanPham " +
                           "from CONGNHAN c, NKSLK n, CHITIETCONGNHAN ctcn, CHITIETCONGVIEC ctcv, SANPHAM s, CONGVIEC cv " +
                           "where c.ID_CONGNHAN = ctcn.ID_CONGNHAN and ctcn.ID_CTCONGVIEC = ctcv.ID_CTCONGVIEC " +
                           "and ctcv.ID_NKSLK = n.ID_NKSLK and ctcv.ID_CONGVIEC = cv.ID_CONGVIEC and ctcv.ID_SANPHAM = s.ID_SANPHAM ";

            // Loc du lieu theo tuan hoac thang
            if (criterion != "All")
            {
                query += "and n.NgayThucHienKhoan between dbo.getFirstDayOf" + criterion + 
                         "(@date) and dbo.getLastDayOf" + criterion + "(@date) ";
                command.Parameters.Add("@date", SqlDbType.DateTime);
                if (criterion == "Week")
                {
                    command.Parameters["@date"].Value = DateTime.ParseExact(selectedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
                }
                else
                {
                    command.Parameters["@date"].Value = DateTime.ParseExact(selectedDate, "MM/yyyy", CultureInfo.InvariantCulture);
                }
            }

            // Loc du lieu theo ma cong nhan
            if (idLabour != 0)
            {
                query += "and c.ID_CONGNHAN = @id ";
                command.Parameters.Add("@id", SqlDbType.Int);
                command.Parameters["@id"].Value = idLabour;
            }

            // Loc du lieu theo tu khoa tim kiem
            if (!string.IsNullOrEmpty(search))
            {
                query += "and (c.HoTen like '%' + @search + '%' " +
                         "or n.NgayThucHienKhoan like '%' + @search + '%' " +
                         "or n.GioBatDau like '%' + @search + '%' " +
                         "or n.GioKetThuc like '%' + @search + '%' " +
                         "or cv.TenCongViec like '%' + @search + '%' " +
                         "or s.TenSanPham like '%' + @search + '%' " +
                         "or ctcv.SanLuong like '%' + @search + '%' " +
                         "or ctcv.SoLoSanPham like '%' + @search + '%') ";
                command.Parameters.Add("@search", SqlDbType.NVarChar);
                command.Parameters["@search"].Value = search;
            }

            // Sap xep du lieu
            switch (order)
            {
                case "ID_asc": query += "order by c.ID_CONGNHAN "; break;
                case "ID_desc": query += "order by c.ID_CONGNHAN desc "; break;
                case "LabourName_asc": query += "order by c.HoTen "; break;
                case "LabourName_desc": query += "order by c.HoTen desc "; break;
                case "Day_desc": query += "order by n.NgayThucHienKhoan desc "; break;
                case "Start_asc": query += "order by ctcn.GioVaoLam "; break;
                case "Start_desc": query += "order by ctcn.GioVaoLam desc "; break;
                case "End_asc": query += "order by ctcn.GioTanLam "; break;
                case "End_desc": query += "order by ctcn.GioTanLam desc "; break;
                case "WorkName_asc": query += "order by cv.TenCongViec "; break;
                case "WorkName_desc": query += "order by cv.TenCongViec desc "; break;
                case "ProductName_asc": query += "order by s.TenSanPham "; break;
                case "ProductName_desc": query += "order by s.TenSanPham desc "; break;
                case "Quantity_asc": query += "order by ctcv.SanLuong "; break;
                case "Quantity_desc": query += "order by ctcv.SanLuong desc "; break;
                case "Batch_asc": query += "order by ctcv.SoLoSanPham "; break;
                case "Batch_desc": query += "order by ctcv.SoLoSanPham desc "; break;
                default: query += "order by n.NgayThucHienKhoan "; break;
            }

            // Tien hanh truy van
            command.CommandText = query;
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new NKSLKModel()
                    {
                        ID_CONGNHAN = "NV" + (reader.GetInt32(0) < 10 ? "00" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0) < 100 ? "0" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0).ToString()),
                        HoTen = reader.GetString(1),
                        NgayThucHienKhoan = reader.GetDateTime(2),
                        GioBatDau = reader.GetTimeSpan(3),
                        GioKetThuc = reader.GetTimeSpan(4),
                        TenCongViec = reader.GetString(5),
                        TenSanPham = reader.GetString(6),
                        SanLuong = reader.GetInt32(7),
                        SoLoSanPham = reader.GetInt32(8)
                    });
                }
            }
            connection.Close();

            // Chuyen sang dang IEnumerable
            IEnumerable<NKSLKModel> model = list;
            return model;
        }

        /*Thong ke bang luong san pham cong nhan theo tuan, thang*/
        public IEnumerable<SalaryModel> GetSalaries(string criterion, string selectedDate, string order)
        {
            List<SalaryModel> list = new List<SalaryModel>();

            // Tao ket noi toi csdl
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;
         
            // Tao cau lenh truy van lay du lieu tu nhung bang lien quan
            string query = "with SOGIOLAM as " +
                            "( " +
                            "select ctcn.ID_CTCONGNHAN, ctcn.ID_CTCONGVIEC, " +
                               "case " +
                                   "when ctcn.GioTanLam > '06:00:00' then DATEDIFF(minute, ctcn.GioVaoLam, ctcn.GioTanLam) * 1.0 / 60 " +
                                   "when ctcn.GioTanLam <= '06:00:00' then(24 * 60 - DATEDIFF(minute, ctcn.GioTanLam, ctcn.GioVaoLam)) * 1.0 / 60 " +
                               "end as 'SoGioLam' " +
                            "from CHITIETCONGNHAN ctcn " +
                            "where ctcn.ID_CTCONGVIEC in " +
                            "(select ID_CTCONGVIEC from CHITIETCONGNHAN group by ID_CTCONGVIEC having count(ID_CTCONGVIEC) > 1) " +
                            "), " +
                            "TONGGIOLAM as " +
                            "( " +
                               "select sgl.ID_CTCONGVIEC, SUM(SoGioLam) as 'TongGioLam' " +
                               "from SOGIOLAM sgl group by sgl.ID_CTCONGVIEC " +
                            "), " +
                            "BANGLUONGSANPHAM as " +
                            "( " +
                                "select ctcn.ID_CONGNHAN, (ctcv.SanLuong * cv.DonGia) as 'LuongSanPham' " +
                                "from CONGVIEC cv, CHITIETCONGVIEC ctcv, CHITIETCONGNHAN ctcn, NKSLK n " +
                                "where cv.ID_CONGVIEC = ctcv.ID_CONGVIEC " +
                                    "and ctcv.ID_NKSLK = n.ID_NKSLK " +
                                    "and ctcv.ID_CTCONGVIEC = ctcn.ID_CTCONGVIEC " +
                                    "and n.NgayThucHienKhoan between dbo.getFirstDayOf" + criterion +
                                    "(@date) and dbo.getLastDayOf" + criterion + "(@date) " +
                                    "and ctcv.ID_CTCONGVIEC in " +
                                    "(select ID_CTCONGVIEC from CHITIETCONGNHAN group by ID_CTCONGVIEC having count(ID_CTCONGVIEC) = 1) " +
                                "union all " +
                                "select ctcn.ID_CONGNHAN, ctcv.SanLuong * cv.DonGia * sgl.SoGioLam / tgl.TongGioLam as 'LuongSanPham' " +
                                "from CONGVIEC cv, CHITIETCONGVIEC ctcv, CHITIETCONGNHAN ctcn, NKSLK n, SOGIOLAM sgl, TONGGIOLAM tgl " +
                                "where cv.ID_CONGVIEC = ctcv.ID_CONGVIEC " +
                                    "and ctcv.ID_CTCONGVIEC = ctcn.ID_CTCONGVIEC " +
                                    "and ctcv.ID_NKSLK = n.ID_NKSLK " +
                                    "and ctcn.ID_CTCONGVIEC = tgl.ID_CTCONGVIEC " +
                                    "and ctcn.ID_CTCONGNHAN = sgl.ID_CTCONGNHAN " +
                                    "and n.NgayThucHienKhoan between dbo.getFirstDayOf" + criterion +
                                    "(@date) and dbo.getLastDayOf" + criterion + "(@date) " +
                            ") " +
                            "select c.ID_CONGNHAN, c.HoTen, ROUND(CONVERT(int, SUM(b.LuongSanPham)), -3) as 'TongLuongSanPham' " +
                            "from CONGNHAN c, BANGLUONGSANPHAM b " +
                            "where c.ID_CONGNHAN = b.ID_CONGNHAN " +
                            "group by c.ID_CONGNHAN, c.HoTen ";

            // Loc du lieu theo tuan hoac thang
            command.Parameters.Add("@date", SqlDbType.DateTime);
            if (criterion == "Week")
            {
                command.Parameters["@date"].Value = DateTime.ParseExact(selectedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            }
            else
            {
                command.Parameters["@date"].Value = DateTime.ParseExact(selectedDate, "MM/yyyy", CultureInfo.InvariantCulture);
            }

            // Sap xep du lieu
            switch (order)
            {
                case "ID_desc": query += "order by c.ID_CONGNHAN desc "; break;
                case "Name_asc": query += "order by c.HoTen "; break;
                case "Name_desc": query += "order by c.HoTen desc "; break;
                case "Salary_asc": query += "order by TongLuongSanPham "; break;
                case "Salary_desc": query += "order by TongLuongSanPham desc "; break;
                default: query += "order by c.ID_CONGNHAN "; break;
            }

            // Tien hanh truy van
            command.CommandText = query;
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new SalaryModel()
                    {
                        ID_CONGNHAN = "NV" + (reader.GetInt32(0) < 10 ? "00" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0) < 100 ? "0" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0).ToString()),
                        HoTen = reader.GetString(1),
                        LuongSanPham = reader.GetInt32(2)
                    });
                }
            }
            connection.Close();

            // Chuyen sang dang IEnumerable
            IEnumerable<SalaryModel> model = list;
            return model;
        }

        /* Hien thi so ngay cong di lam trong thang cua cong nhan */       
        public IEnumerable<WorkdayModel> GetWorkdays(string selectedDate, int idLabour, string order)
        {
            List<WorkdayModel> list = new List<WorkdayModel>();

            // Tao ket noi toi csdl
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;

            // Loc du lieu theo ma cong nhan
            string subquery = "";
            if (idLabour != 0)
            {
                subquery = "and ctcn.ID_CONGNHAN = @id ";
                command.Parameters.Add("@id", SqlDbType.Int);
                command.Parameters["@id"].Value = idLabour;
            }

            // Tao cau lenh truy van lay du lieu tu nhung bang lien quan
            string query = "with BANGNGAYCONG as " +
                           "( " +
                                "select ctcn.ID_CONGNHAN, " +
                                    "case " +
                                        "when ctcn.GioTanLam > '06:00:00' then DATEDIFF(minute, ctcn.GioVaoLam, ctcn.GioTanLam) * 1.0 / 60 / 8 " +
                                        "when ctcn.GioTanLam <= '06:00:00' then(24 * 60 - DATEDIFF(minute, ctcn.GioTanLam, ctcn.GioVaoLam)) * 1.0 / 60 * 1.3 / 8 " +
                                    "end as 'NgayCong' " +
                                "from CHITIETCONGNHAN ctcn, CHITIETCONGVIEC ctcv, NKSLK n " +
                                "where ctcn.ID_CTCONGVIEC = ctcv.ID_CTCONGVIEC and ctcv.ID_NKSLK = n.ID_NKSLK " +
                                  "and n.NgayThucHienKhoan between dbo.getFirstDayOfMonth(@date) and dbo.getLastDayOfMonth(@date) " +
                                  subquery +
                           ") " +
                           "select c.ID_CONGNHAN, c.HoTen, CONVERT(DECIMAL(5, 2), SUM(b.NgayCong)) as 'TongSoNgayCong' " +
                           "from CONGNHAN c, BANGNGAYCONG b " +
                           "where c.ID_CONGNHAN = b.ID_CONGNHAN " +
                           "group by c.ID_CONGNHAN, c.HoTen ";
                           
            // Loc du lieu theo thang
            command.Parameters.Add("@date", SqlDbType.DateTime);
            command.Parameters["@date"].Value = DateTime.ParseExact(selectedDate, "MM/yyyy", CultureInfo.InvariantCulture);

            // Sap xep du lieu
            switch (order)
            {
                case "ID_desc": query += "order by c.ID_CONGNHAN desc "; break;
                case "Name_asc": query += "order by c.HoTen "; break;
                case "Name_desc": query += "order by c.HoTen desc "; break;
                case "Workday_asc": query += "order by TongSoNgayCong "; break;
                case "Workday_desc": query += "order by TongSoNgayCong desc "; break;
                default: query += "order by c.ID_CONGNHAN "; break;
            }

            // Tien hanh truy van
            command.CommandText = query;
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new WorkdayModel()
                    {
                        ID_CONGNHAN = "NV" + (reader.GetInt32(0) < 10 ? "00" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0) < 100 ? "0" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0).ToString()),
                        HoTen = reader.GetString(1),
                        TongSoNgayCong = Convert.ToDouble(reader.GetDecimal(2))
                    });
                }
            }
            connection.Close();

            // Chuyen sang dang IEnumerable
            IEnumerable <WorkdayModel> model = list;
            return model;
        }

        /* Thong ke danh muc cong nhan co so gio cong trong tuan vuot chuan */
        public IEnumerable<WorkinghourModel> GetWorkinghours(string selectedDate, string order)
        {
            List<WorkinghourModel> list = new List<WorkinghourModel>();

            // Tao ket noi toi csdl
            SqlConnection connection = new SqlConnection(connectionString);
            connection.Open();
            SqlCommand command = new SqlCommand();
            command.Connection = connection;

            // Tao cau lenh truy van lay du lieu tu nhung bang lien quan
            string query = "with SOGIOLAM as " +
                           "( " +
                            "select ctcn.ID_CONGNHAN, " +
                                "case " +
                                    "when ctcn.GioTanLam > '06:00:00' then DATEDIFF(minute, ctcn.GioVaoLam, ctcn.GioTanLam) *1.0 / 60 " +
                                    "when ctcn.GioTanLam <= '06:00:00' then (24 * 60 - DATEDIFF(minute, ctcn.GioTanLam, ctcn.GioVaoLam)) * 1.0 / 60 " +
                                "end as 'SoGioLam' " +
                            "from CHITIETCONGNHAN ctcn, CHITIETCONGVIEC ctcv, NKSLK n " +
                            "where ctcn.ID_CTCONGVIEC = ctcv.ID_CTCONGVIEC and ctcv.ID_NKSLK = n.ID_NKSLK " +
                                "and n.NgayThucHienKhoan between dbo.getFirstDayOfWeek(@date) and dbo.getLastDayOfWeek(@date) " +
                           ") " +
                           "select c.ID_CONGNHAN, c.HoTen, CONVERT(DECIMAL(5, 2), SUM(s.SoGioLam)) as 'TongGioLamTrongTuan' " +
                           "from CONGNHAN c, SOGIOLAM s " +
                           "where c.ID_CONGNHAN = s.ID_CONGNHAN " +
                           "group by c.ID_CONGNHAN, c.HoTen " +
                           "having SUM(s.SoGioLam) > dbo.getSoGioMax(@date) ";

            // Loc du lieu theo tuan
            command.Parameters.Add("@date", SqlDbType.DateTime);
            command.Parameters["@date"].Value = DateTime.ParseExact(selectedDate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            // Sap xep du lieu
            switch (order)
            {
                case "ID_desc": query += "order by c.ID_CONGNHAN desc "; break;
                case "Name_asc": query += "order by c.HoTen "; break;
                case "Name_desc": query += "order by c.HoTen desc "; break;
                case "Workinghour_asc": query += "order by TongGioLamTrongTuan "; break;
                case "Workinghour_desc": query += "order by TongGioLamTrongTuan desc "; break;
                default: query += "order by c.ID_CONGNHAN "; break;
            }

            // Tien hanh truy van
            command.CommandText = query;
            SqlDataReader reader = command.ExecuteReader();
            if (reader.HasRows)
            {
                while (reader.Read())
                {
                    list.Add(new WorkinghourModel()
                    {
                        ID_CONGNHAN = "NV" + (reader.GetInt32(0) < 10 ? "00" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0) < 100 ? "0" + reader.GetInt32(0).ToString() :
                                              reader.GetInt32(0).ToString()),
                        HoTen = reader.GetString(1),
                        TongGioLam = Convert.ToDouble(reader.GetDecimal(2))
                    });
                }
            }
            connection.Close();

            // Chuyen sang dang IEnumerable
            IEnumerable <WorkinghourModel> model = list;
            return model;
        }
    }
}