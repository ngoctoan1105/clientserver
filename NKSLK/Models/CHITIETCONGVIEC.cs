namespace NKSLK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CHITIETCONGVIEC")]
    public partial class CHITIETCONGVIEC
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CHITIETCONGVIEC()
        {
            CHITIETCONGNHANs = new HashSet<CHITIETCONGNHAN>();
        }

        [Key]
        public int ID_CTCONGVIEC { get; set; }

        public int ID_NKSLK { get; set; }

        public int ID_CONGVIEC { get; set; }

        public int ID_SANPHAM { get; set; }

        public int? SanLuong { get; set; }

        public int? SoLoSanPham { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CHITIETCONGNHAN> CHITIETCONGNHANs { get; set; }

        public virtual CONGVIEC CONGVIEC { get; set; }

        public virtual NKSLK NKSLK { get; set; }

        public virtual SANPHAM SANPHAM { get; set; }
    }
}
