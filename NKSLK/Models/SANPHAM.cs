namespace NKSLK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("SANPHAM")]
    public partial class SANPHAM
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public SANPHAM()
        {
            CHITIETCONGVIECs = new HashSet<CHITIETCONGVIEC>();
        }

        [Key]
        public int ID_SANPHAM { get; set; }

        [StringLength(50)]
        public string TenSanPham { get; set; }

        [StringLength(50)]
        public string SoDangKy { get; set; }

        [StringLength(50)]
        public string QuyCach { get; set; }

        [Column(TypeName = "date")]
        public DateTime? NgayDangKy { get; set; }

        [Column(TypeName = "date")]
        public DateTime? HanSuDung { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CHITIETCONGVIEC> CHITIETCONGVIECs { get; set; }
    }
}
