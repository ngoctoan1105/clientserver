using System;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Linq;

namespace NKSLK.Models
{
    public partial class NKSLKDbContext : DbContext
    {
        public NKSLKDbContext()
            : base("name=NKSLK_Model")
        {
        }

        public virtual DbSet<ACCOUNT> ACCOUNTs { get; set; }
        public virtual DbSet<CHITIETCONGNHAN> CHITIETCONGNHANs { get; set; }
        public virtual DbSet<CHITIETCONGVIEC> CHITIETCONGVIECs { get; set; }
        public virtual DbSet<CHUCVU> CHUCVUs { get; set; }
        public virtual DbSet<CONGNHAN> CONGNHANs { get; set; }
        public virtual DbSet<CONGVIEC> CONGVIECs { get; set; }
        public virtual DbSet<NKSLK> NKSLKs { get; set; }
        public virtual DbSet<PHONGBAN> PHONGBANs { get; set; }
        public virtual DbSet<SANPHAM> SANPHAMs { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ACCOUNT>()
                .Property(e => e.TenAccount)
                .IsUnicode(false);

            modelBuilder.Entity<ACCOUNT>()
                .Property(e => e.MatKhau)
                .IsUnicode(false);

            modelBuilder.Entity<CHITIETCONGNHAN>()
                .Property(e => e.GioVaoLam)
                .HasPrecision(0);

            modelBuilder.Entity<CHITIETCONGNHAN>()
                .Property(e => e.GioTanLam)
                .HasPrecision(0);

            modelBuilder.Entity<CHITIETCONGVIEC>()
                .HasMany(e => e.CHITIETCONGNHANs)
                .WithRequired(e => e.CHITIETCONGVIEC)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CONGNHAN>()
                .HasMany(e => e.CHITIETCONGNHANs)
                .WithRequired(e => e.CONGNHAN)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<CONGVIEC>()
                .Property(e => e.HeSoKhoan)
                .HasPrecision(3, 2);

            modelBuilder.Entity<CONGVIEC>()
                .HasMany(e => e.CHITIETCONGVIECs)
                .WithRequired(e => e.CONGVIEC)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<NKSLK>()
                .Property(e => e.GioBatDau)
                .HasPrecision(0);

            modelBuilder.Entity<NKSLK>()
                .Property(e => e.GioKetThuc)
                .HasPrecision(0);

            modelBuilder.Entity<NKSLK>()
                .HasMany(e => e.CHITIETCONGVIECs)
                .WithRequired(e => e.NKSLK)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<SANPHAM>()
                .Property(e => e.SoDangKy)
                .IsUnicode(false);

            modelBuilder.Entity<SANPHAM>()
                .HasMany(e => e.CHITIETCONGVIECs)
                .WithRequired(e => e.SANPHAM)
                .WillCascadeOnDelete(false);
        }
    }
}
