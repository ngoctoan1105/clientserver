namespace NKSLK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("ACCOUNT")]
    public partial class ACCOUNT
    {
        [Key]
        public int ID_ACCOUNT { get; set; }

        [StringLength(50)]
        public string TenAccount { get; set; }

        [StringLength(50)]
        public string MatKhau { get; set; }
    }
}
