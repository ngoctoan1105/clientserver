namespace NKSLK.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Data.Entity.Spatial;

    [Table("CONGNHAN")]
    public partial class CONGNHAN
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public CONGNHAN()
        {
            CHITIETCONGNHANs = new HashSet<CHITIETCONGNHAN>();
        }

        [Key]
        public int ID_CONGNHAN { get; set; }

        [StringLength(50)]
        public string HoTen { get; set; }

        [Column(TypeName = "date")]
        public DateTime? NgaySinh { get; set; }

        public bool? GioiTinh { get; set; }

        [StringLength(50)]
        public string QueQuan { get; set; }

        public int? LuongHopDong { get; set; }

        public int? LuongBaoHiem { get; set; }

        public int? ID_PHONGBAN { get; set; }

        public int? ID_CHUCVU { get; set; }

        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CHITIETCONGNHAN> CHITIETCONGNHANs { get; set; }

        public virtual CHUCVU CHUCVU { get; set; }

        public virtual PHONGBAN PHONGBAN { get; set; }
    }
}
