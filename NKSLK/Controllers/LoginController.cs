﻿using NKSLK.DAO;
using NKSLK.EF;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace NKSLK.Controllers
{
    public class LoginController : Controller
    {
        LoginDAO dao = new LoginDAO();

        public ActionResult Index(string ReturnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Statistics");
            }
            else
            {
                HttpCookie userInfo = Request.Cookies["userInfo"];
                if (userInfo != null)
                {
                    ViewBag.Username = userInfo["Username"].ToString();
                    ViewBag.Password = userInfo["Password"].ToString();
                }
            }         
            if (ReturnUrl != null)
            {
                ViewBag.ReturnUrl = ReturnUrl;
            }
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(LoginModel model, string ReturnUrl)
        {           
            if (ModelState.IsValid)
            {
                if (dao.IsValidate(model.Username, model.Password))
                {
                    HttpCookie userInfo = new HttpCookie("userInfo");
                    if (model.RememberMe)
                    {                                         
                        userInfo["Username"] = model.Username;
                        userInfo["Password"] = model.Password;
                        userInfo.Expires = DateTime.Now.AddHours(6);
                        Response.Cookies.Add(userInfo);
                    }
                    else
                    {
                        userInfo.Expires = DateTime.Now.AddSeconds(-1);
                        Response.Cookies.Add(userInfo);
                    }
                    FormsAuthentication.SetAuthCookie(model.Username, model.RememberMe);
                    return RedirectToLocal(ReturnUrl);
                }
                else
                {
                    ModelState.AddModelError("", "Tên đăng nhập hoặc mật khẩu không chính xác");
                }
            }

            return View(model);
        }

        public ActionResult RedirectToLocal(string ReturnUrl)
        {
            if (Url.IsLocalUrl(ReturnUrl))
            {
                return Redirect(ReturnUrl);
            }
            else
            {
                return RedirectToAction("Index", "Statistics");
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index");
        }

        public ActionResult ForgetPassword()
        {
            return View();
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ForgetPassword(ForgetPasswordModel model)
        {
            if (ModelState.IsValid)
            {
                if (dao.IsUserExisted(model.Username))
                {
                    if (model.NewPass.Equals(model.ConfirmedPass))
                    {
                        string callbackUrl = Url.Action("ResetPassword", "Login", 
                                                     new { username = model.Username, newpass = model.NewPass }, Request.Url.Scheme);
                        dao.SendResetPasswordEmail(model.Email, callbackUrl);
                        TempData["AlertMessage"] = "Kiểm tra Email để xác thực thay đổi mật khẩu";
                        TempData["AlertType"] = "alert-success";
                    }
                    else
                    {
                        ModelState.AddModelError("", "Mật khẩu xác nhận không chính xác");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Tên tài khoản không tồn tại");
                }
            }
            return View(model);
        }

        public ActionResult ResetPassword(string username, string newpass)
        {
            dao.ResetPassword(username, newpass);
            return RedirectToAction("Index");
        }
    }
}