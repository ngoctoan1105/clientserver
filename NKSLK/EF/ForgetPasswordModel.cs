﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NKSLK.EF
{
    public class ForgetPasswordModel
    {
        [Display(Name = "Tên tài khoản")]
        [Required(ErrorMessage = "Bạn chưa nhập tên tài khoản")]
        public string Username { get; set; }

        [Display(Name = "Mật khẩu mới")]
        [Required(ErrorMessage = "Bạn chưa nhập mật khẩu mới")]
        public string NewPass { get; set; }

        [Display(Name = "Xác nhận mật khẩu")]
        [Required(ErrorMessage = "Bạn chưa nhập mật khẩu xác nhận")]
        public string ConfirmedPass { get; set; }

        [Display(Name = "Email")]
        [Required(ErrorMessage = "Bạn chưa nhập Email")]
        [DataType(DataType.EmailAddress)]
        [RegularExpression(@"[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}", ErrorMessage = "Email không hợp lệ")]
        public string Email { get; set; }
    }
}