﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace NKSLK.EF
{
    public class SalaryModel
    {
        [Display(Name = "Mã công nhân")]
        public string ID_CONGNHAN { get; set; }

        [Display(Name = "Họ tên")]
        public string HoTen { get; set; }

        [Display(Name = "Lương sản phẩm")]
        public int? LuongSanPham { get; set; }
    }
}